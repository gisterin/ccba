﻿﻿$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    loop: true,
    items: 1,
    center: true,
    nav: true,
    navText: [
      "<i class='slider-arrow-left'></i>",
      "<i class='slider-arrow-right'></i>"
    ]
  });

  $('ul#main-menu li.has-sub-menu').on('mouseenter', function () {
    $(this).find('.sub-menu').show();
  });

  $('ul#main-menu li.has-sub-menu').on('mouseleave', function () {
    $(this).find('.sub-menu').hide();
  });

  $('button#see-it-in-action').on('click', function () {
    window.location.href = "/Contact-Us";
  });

  $('button#platform-link').on('click', function () {
    window.location.href = "/Platform";
  });

  $('button#analytics-link').on('click', function () {
    window.location.href = "/Analytics";
  });

  $('form#contact-form').on('submit', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var data = { 
      firstName: $('input[name="firstName"]').val(),
      lastName: $('input[name="lastName"]').val(),
      email: $('input[name="email"]').val(),
      company: $('input[name="company"]').val(),
      telephone: $('input[name="telephone"]').val(),
      comments: $('textarea[name="comments"]').val()
    };
    
    $.post("/contact", data, function (response) {
      if (response.status === 200) {
        $('input[name="firstName"]').val('');
        $('input[name="lastName"]').val('');
        $('input[name="email"]').val('');
        $('input[name="company"]').val('');
        $('input[name="telephone"]').val('');
        $('textarea[name="comments"]').val('');
        $('span.success-msg').text('Email Sent!');
      }
    });
  });
  
});
