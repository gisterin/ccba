﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using MailKit.Net.Smtp;
using MimeKit;

namespace crosscurrentmvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Title"] = "Home";

            return View();
        }

		public ActionResult Analytics()
		{
			ViewData["Title"] = "Analytics";
			return View();
		}

		public ActionResult Platform()
		{
			ViewData["Title"] = "Platform";
			return View();
		}

        [ActionName("Contact-Us")]
        public ActionResult ContactUs()
        {
            ViewData["Title"] = "Contact Us";
            return View();
        }

		[HttpPost]
		public JsonResult Contact()
		{
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("crosscurrentcontact@gmail.com"));
            message.To.Add(new MailboxAddress("info@crosscurrent.com"));
            message.Subject = "Crosscurrent.com - Contact Form Submission";
            message.Body = new TextPart("html")
            {
                Text = "Someone filled out the contact form on www.crosscurrent.com!<br /><br /><br />" +
                    "First Name: " + Request["firstName"].ToString() + "<br />" +
                    "Last Name: " + Request["lastName"].ToString() + "<br />" +
                    "Company: " + Request["company"].ToString() + "<br />" +
                    "Email: " + Request["email"].ToString() + "<br />" +
                    "Telephone: " + Request["telephone"].ToString() + "<br />" +
                    "Comments: " + Request["comments"].ToString() 
            };
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("cc_services@crosscurrent.com", "cc!Spring2013"); // TODO: Gennadiy, change to crosscurrent smtp?
                client.Send(message);
                client.Disconnect(false);
            }
            var result = new { success = "Contacted Successfully", status = 200 };
            return Json(result, JsonRequestBehavior.AllowGet);
		}
    }
}
