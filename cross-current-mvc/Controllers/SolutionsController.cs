﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crosscurrentmvc.Controllers
{
    public class SolutionsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Analytics()
		{
            ViewData["Title"] = "Analytics";
			return View();
		}

        public ActionResult Platform()
        {
            ViewData["Title"] = "Platform";
            return View();
        }
    }
}
