﻿var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./Styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./Content/stylesheets'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./Styles/**/*.scss', ['sass']);
});